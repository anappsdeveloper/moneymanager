package com.krishnanepal.moneymanager.data.repo

import com.krishnanepal.moneymanager.data.local.IncomeExpensesDao
import com.krishnanepal.moneymanager.data.local.IncomeExpensesEntity
import com.krishnanepal.moneymanager.data.model.DailyModel
import kotlinx.coroutines.flow.Flow
import java.util.*
import javax.inject.Inject

interface IncomeExpensesRepo {

    suspend fun insertBothEntity(incomeExpensesEntity: IncomeExpensesEntity):Long

    suspend fun updateIncomeExpensesEntity(incomeExpensesEntity: IncomeExpensesEntity)

     fun getIncomeExpensesEntity(): Flow<List<IncomeExpensesEntity>>

    suspend fun deleteIncomeExpensesEntityById(id:Int)

    fun getDailyIncomeExpensesEntity():Flow<List<DailyModel>>

   suspend fun getIncomeExpensesByDateEntity(date: Date):List<IncomeExpensesEntity>
    /*  suspend fun insertIncomeEntity(income:Income)*/
}

class IncomeExpensesRepoImp @Inject constructor(
    private val incomeExpensesDao: IncomeExpensesDao
): IncomeExpensesRepo {
    override suspend fun insertBothEntity(incomeExpensesEntity: IncomeExpensesEntity): Long {
        return incomeExpensesDao.insertBothEntity(incomeExpensesEntity)
    }

    override suspend fun updateIncomeExpensesEntity(entity: IncomeExpensesEntity) {
//       incomeExpensesDao.updateIncomeExpensesEntityById(entity.id,entity.item,entity.amt,entity.cat,entity.date,entity.isExpenses)
    }

    override fun getIncomeExpensesEntity(): Flow<List<IncomeExpensesEntity>> {
        return incomeExpensesDao.getIncomeExpensesEntity()
    }

    override suspend fun deleteIncomeExpensesEntityById(id: Int) {
        incomeExpensesDao.deleteIncomeExpensesEntityById(id)
    }

    override fun getDailyIncomeExpensesEntity(): Flow<List<DailyModel>> {
      return incomeExpensesDao.getDailyIncomeExpensesEntity()
    }

    override suspend fun getIncomeExpensesByDateEntity(date: Date): List<IncomeExpensesEntity> {
        return incomeExpensesDao.getIncomeExpensesByDateEntity(date)
    }
}