package com.krishnanepal.moneymanager.data.model

import java.util.*

data class DailyModel(
    val date:Date,
    val sum:Int,
    val isExpenses:Boolean
)