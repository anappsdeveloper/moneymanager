package com.krishnanepal.moneymanager.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.krishnanepal.moneymanager.data.model.DailyModel
import kotlinx.coroutines.flow.Flow
import java.util.*

@Dao
interface IncomeExpensesDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertBothEntity(incomeExpensesEntity: IncomeExpensesEntity): Long

    @Query("SELECT * FROM IncomeExpensesEntity ORDER BY date DESC")
    fun getIncomeExpensesEntity(): Flow<List<IncomeExpensesEntity>>

    @Query("DELETE FROM IncomeExpensesEntity WHERE id = :id")
    suspend fun deleteIncomeExpensesEntityById(id: Int)

  /*  @Query("UPDATE IncomeExpensesEntity SET item = :item, amt = :amt, cat = :cat, date = :date, isExpenses = :isExpenses WHERE id = :id")
    suspend fun updateIncomeExpensesEntityById(id:Int, item:String, amt:Double, cat:String, date:String, isExpenses:Boolean)*/

    //DateWise
    @Query("SELECT date,SUM(amt) AS sum,isExpenses FROM IncomeExpensesEntity GROUP BY date,isExpenses ORDER BY date DESC")
//    @Query("select date, sum(amt)sum, isExpenses from IncomeExpensesEntity  group by strftime('%Y-%m-%d', date / 1000, 'unixepoch','localtime'), isExpenses ORDER BY date DESC")
    fun getDailyIncomeExpensesEntity(): Flow<List<DailyModel>>

     //Daily on the basic of  Date
     @Query("SELECT * FROM IncomeExpensesEntity WHERE date = :date ORDER BY date DESC")
    suspend fun getIncomeExpensesByDateEntity(date: Date): List<IncomeExpensesEntity>

    //current month 01 upto today data
/*    select  strftime('%m','now')month, sum(amt)sum, isExpenses from IncomeExpensesEntity where strftime('%Y-%m-%d', date / 1000, 'unixepoch','localtime') Between DATE('now',
    'start of month'
    )AND DATE('now') GROUP BY isExpenses*/

    //previous month 01 to 31 data
/*select  strftime('%m','now','-1 month'), sum(amt)amt, isExpenses from IncomeExpensesEntity where strftime('%Y-%m-%d', date / 1000, 'unixepoch','localtime') Between DATE('now',
                                                                                                                                                                                                                                                '-1 month',
                                                                                                                                                                                                                                                 'start of month'
                                                                                                                                                                                                                                             )AND DATE('now',
                                                                                                                                                                                                                                             '-1 month',
 'start of month',
                                                                                                                             '+1 month',

                                                                                                                             '-1 day') GROUP BY isExpenses*/

//month wise total income and expenses
/* select strftime('%Y-%m', date / 1000, 'unixepoch','localtime'), sum(amt)amt, isExpenses from IncomeExpensesEntity  group by strftime('%Y-%m', date / 1000, 'unixepoch','localtime'), isExpenses*/

    //date wise total income and expenses
    /* select strftime('%Y-%m-%d', date / 1000, 'unixepoch','localtime') date, sum(amt)amt, isExpenses from IncomeExpensesEntity  group by strftime('%Y-%m-%d', date / 1000, 'unixepoch','localtime'), isExpenses*/
}