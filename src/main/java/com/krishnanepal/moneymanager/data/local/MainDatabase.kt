package com.krishnanepal.moneymanager.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.krishnanepal.moneymanager.utils.DateConverter

@Database(entities = arrayOf(IncomeExpensesEntity::class),
    version = 6,exportSchema = false)
@TypeConverters(DateConverter::class)
abstract class MainDatabase: RoomDatabase(){

    abstract fun incomeExpensesDao(): IncomeExpensesDao

    companion object{
        const val DATABASE_NAME:String = "main_db"
    }
}