package com.krishnanepal.moneymanager.data.local

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import java.util.*

@Entity
data class IncomeExpensesEntity(
    @PrimaryKey(autoGenerate = true)
    var id: Int=0,/** here always put var for autoincrement */

    val item: String,
    val amt:Double,
    val cat:String,
    val date: Date,
    val isExpenses:Boolean
)

/*
fun BothEntity.toBoth(): Both {
    return Both(
        id = id,
        item = item,
        item_pic = item_pic,
        amt = amt,
        cat = cat,
        date = date,
        expenses = expenses
    )
}*/
