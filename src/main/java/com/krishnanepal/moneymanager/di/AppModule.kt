package com.krishnanepal.moneymanager.di

import android.content.Context
import androidx.room.Room
import com.krishnanepal.moneymanager.R
import com.krishnanepal.moneymanager.data.local.IncomeExpensesDao
import com.krishnanepal.moneymanager.data.local.MainDatabase
import com.krishnanepal.moneymanager.data.repo.IncomeExpensesRepo
import com.krishnanepal.moneymanager.data.repo.IncomeExpensesRepoImp
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton //singleton means create instance only once
    fun provideMainDataBase(@ApplicationContext context: Context) : MainDatabase {
        return Room.databaseBuilder(
            context,
            MainDatabase::class.java,
            MainDatabase.DATABASE_NAME
        )
            .fallbackToDestructiveMigration()
            .build()
    }

    @Provides
    @Singleton
    fun provideIncomeExpensesDao(mainDatabase: MainDatabase) : IncomeExpensesDao {
        return mainDatabase.incomeExpensesDao()
    }

    @Provides
    @Singleton
    fun provideMainRepo(incomeExpensesDao: IncomeExpensesDao): IncomeExpensesRepo {
        return IncomeExpensesRepoImp(incomeExpensesDao)
    }


    @Singleton
    @Provides
    @Named("error")
    fun  errorMessage(@ApplicationContext context: Context) = context.getString(R.string.error)

    @Singleton
    @Provides
    @Named("info")
    fun  infoMessage() = "This is info message"
}