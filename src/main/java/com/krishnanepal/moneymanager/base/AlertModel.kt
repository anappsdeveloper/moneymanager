package com.krishnanepal.moneymanager.base

data class AlertMultiActionModel(
    var title: String,
    var message: String,
    var positiveText: String,
    var negativeText: String,
    var positiveAction: () -> Unit = {},
    var negativeAction: () -> Unit = {}
)

data class AlertSingleActionModel(
    var message: String,
    var positiveText: String,
    var positiveAction: () -> Unit = {}
)