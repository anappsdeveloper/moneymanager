package com.krishnanepal.moneymanager.base

import androidx.lifecycle.ViewModel
import com.krishnanepal.moneymanager.utils.livedata.SingleLiveEvent

open abstract class BaseViewModel : ViewModel() {

    val errorEvent = SingleLiveEvent<String>()
    val infoEvent = SingleLiveEvent<String>()
    val successEvent = SingleLiveEvent<String>()
    val toastEvent = SingleLiveEvent<String>()

    val loadingEvent =  SingleLiveEvent<Boolean>()

    val alertDialogEvent = SingleLiveEvent<String>()
    val alertDialogSingleEvent = SingleLiveEvent<AlertSingleActionModel>()
    val alertDialogMultiEvent = SingleLiveEvent<AlertMultiActionModel>()

    fun showError(msg:String){
        errorEvent.value = msg
    }

    fun showSuccess(msg:String){
        successEvent.value = msg
    }

    fun showInfo(msg:String){
        infoEvent.value = msg
    }

    fun showToast(msg:String){
        toastEvent.value = msg
    }

    fun showLoading(){
        loadingEvent.value = true
    }

    fun hideLoading(){
        loadingEvent.value = false
    }

    fun showAlertDialog(msg:String){
        alertDialogEvent.value = msg
    }

    fun showAlertDialog(
        msg:String,
        positiveTxt:String,
        positiveAction:() -> Unit = {}
    ){
        alertDialogSingleEvent.value = AlertSingleActionModel(
            message = msg,
            positiveText = positiveTxt,
            positiveAction = positiveAction)
    }

    fun showAlertDialog(
        title: String,
        msg: String,
        positiveTxt: String,
        negativeTxt: String,
        positiveAction: () -> Unit = {},
        negativeAction: () -> Unit = {}
    ){
       alertDialogMultiEvent.value = AlertMultiActionModel(
           title= title,
           message = msg,
           positiveText = positiveTxt,
           positiveAction = positiveAction,
           negativeText = negativeTxt,
           negativeAction = negativeAction)
    }
}