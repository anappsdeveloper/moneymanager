package com.krishnanepal.moneymanager.base

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.krishnanepal.moneymanager.BR
import com.krishnanepal.moneymanager.utils.Constants
import com.krishnanepal.moneymanager.utils.constracts.AppContract
import com.krishnanepal.moneymanager.utils.constracts.AppContract.String.DATE_FORMAT
import com.krishnanepal.moneymanager.utils.extentions.setupUI
import com.krishnanepal.moneymanager.utils.extentions.showAlertDialog
import com.krishnanepal.moneymanager.utils.extentions.showToast
import com.krishnanepal.moneymanager.utils.view_utils.CustomToast
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

abstract class BaseFragment<DATA_BINDING : ViewDataBinding, VIEW_MODEL : BaseViewModel> :
    Fragment() {
        lateinit var binding: DATA_BINDING
    private var baseViewModel: VIEW_MODEL? = null
    //preference manager
    private var loading: AppLoading = AppLoading()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        performDataBinding(inflater, container)
        initObservers()

//        mGoogleSignInClient = (requireActivity().application as MainApp).mGoogleSignInClient
        return binding.root
    }

    private fun performDataBinding(inflater: LayoutInflater, container: ViewGroup?) {
        binding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false)
        this.baseViewModel = baseViewModel ?: getViewModel()
        binding.apply {
//            setVariable(getBindingVariable(), baseViewModel)
            lifecycleOwner = viewLifecycleOwner
            executePendingBindings()
        }
        activity?.setupUI(binding.root)
    }

    private fun initObservers() {
        baseViewModel?.apply {

            toastEvent.observe(viewLifecycleOwner, Observer {
              activity?.showToast(it)
            })

            successEvent.observe(viewLifecycleOwner, Observer {
              showToastMessage(it,Constants.Strings.SUCCESS)
            })

            infoEvent.observe(viewLifecycleOwner, Observer {
                showToastMessage(it,Constants.Strings.INFO)
            })

            errorEvent.observe(viewLifecycleOwner, Observer {
                showToastMessage(it,Constants.Strings.ERROR)
            })

            alertDialogEvent.observe(viewLifecycleOwner,Observer{
                activity?.showAlertDialog(it)
            })

            alertDialogSingleEvent.observe(viewLifecycleOwner,Observer{
                activity?.showAlertDialog(it.message, it.positiveText, it.positiveAction)
            })

            alertDialogMultiEvent.observe(viewLifecycleOwner,Observer{
                activity?.showAlertDialog(
                    message = it.message,
                    positiveButton = it.positiveText,
                    negativeButton = it.negativeText,
                    positiveAction = it.positiveAction,
                    negativeAction = it.negativeAction
                )
            })

            loadingEvent.observe(requireActivity(), Observer {
                if (it) {
                    loading = AppLoading()
                    loading.isCancelable = false
                    loading.show(requireFragmentManager(), "progress")
                } else {
                    if (loading != null
                        && loading.getDialog() != null
                        && loading.getDialog()!!.isShowing()
                        && !loading.isRemoving()
                    ) {
                        loading.dismiss()
                    } else {
                        loading.dismiss()
                        //dialog is not showing
                    }
                }
            })

        }
    }

     fun showToastMessage(message: String, type: String) {
        when (type) {
            Constants.Strings.SUCCESS -> {
                CustomToast(requireActivity()).showSuccessToast(message)
            }
            Constants.Strings.ERROR -> {
                CustomToast(requireActivity()).showErrorToast(message)
            }
            Constants.Strings.INFO -> {
                CustomToast(requireActivity()).showInfoToast(message)
            }
        }
    }

    fun getCurrentDate():String{
        val c = Calendar.getInstance()
        val sdf = SimpleDateFormat(DATE_FORMAT,Locale.US)
        return sdf.format(c.time)

}

    @LayoutRes
    abstract fun getLayoutId(): Int

    abstract fun getViewModel(): VIEW_MODEL

    open fun getBindingVariable(): Int = BR._all

}