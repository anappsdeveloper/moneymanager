package com.krishnanepal.moneymanager.common

enum class CategoryEnum{
    Other,
    Food,
    Education,
    Fair,
    Clothes,
    Health,
    Grocery

}

enum class CatIncomeEnum{
    Other,
    Salary,
    PartTime
}