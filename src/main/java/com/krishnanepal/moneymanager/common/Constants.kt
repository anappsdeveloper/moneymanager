package com.krishnanepal.moneymanager.common

import android.Manifest

class Constants {


    object Integers {
        const val IMAGE_CAPTURE_CODE = 1
        const val VIDEO_CAPTURE_CODE = 11
        const val AUDIO_CAPTURE_CODE = 111

        const val CAMERA_PERMISSION = 1111
        const val READ_STORAGE_PERMISSION = 2222
        const val WRITE_STORAGE_PERMISSION = 3333
        const val ALL_PERMISSIONS = 4444
        val PERMISSIONS = {
            Manifest.permission.READ_EXTERNAL_STORAGE;Manifest.permission.ACCESS_FINE_LOCATION;
            Manifest.permission.ACCESS_COARSE_LOCATION
        }
    }


    object Strings {
        const val SUCCESS = "success"
        const val OK = "Ok"
        const val CANCEL = "Cancel"
        const val FOLDER_NAME = "ScannedFile"
        const val MOBILE = "Mobile"
        const val IMAGE_PATH = "ImagePath"
        const val FOLDER_ID = "folderId"
        const val CREATE_FOLDER = "CreateFolder"
    }
}



