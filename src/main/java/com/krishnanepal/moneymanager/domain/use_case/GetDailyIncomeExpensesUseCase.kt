package com.krishnanepal.moneymanager.domain.use_case

import com.krishnanepal.moneymanager.data.repo.IncomeExpensesRepo
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GetDailyIncomeExpensesUseCase @Inject constructor(
    private val repo: IncomeExpensesRepo
) {
    operator fun invoke() = flow{
        repo.getDailyIncomeExpensesEntity().collect {
            emit(it)
        }
    }
}