package com.krishnanepal.moneymanager.domain.use_case

import com.krishnanepal.moneymanager.data.repo.IncomeExpensesRepo

import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GetIncomeExpensesUseCase @Inject constructor(
    private val repo: IncomeExpensesRepo
){
  /*  //invoke: execute class as a function
    operator fun invoke(): Flow<List<BothEntity>>{
      *//*  try {*//*
         *//*  emit(Resource.Loading)*//*
*//*
            repo.getBothEntity().collect { listEntity ->
                emit(listEntity)*//*
        return repo.getBothEntity()
            }*/

    operator fun invoke() = flow{
        repo.getIncomeExpensesEntity().collect {
            emit(it)
        }
    }

    }

