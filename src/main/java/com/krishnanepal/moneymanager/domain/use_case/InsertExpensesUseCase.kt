package com.krishnanepal.moneymanager.domain.use_case

import com.krishnanepal.moneymanager.data.local.IncomeExpensesEntity
import com.krishnanepal.moneymanager.data.repo.IncomeExpensesRepo

import javax.inject.Inject

class InsertExpensesUseCase  @Inject constructor(
    private val repo: IncomeExpensesRepo
){
    suspend operator fun invoke(incomeExpenses: IncomeExpensesEntity) {
        repo.insertBothEntity(incomeExpenses)
    }
}