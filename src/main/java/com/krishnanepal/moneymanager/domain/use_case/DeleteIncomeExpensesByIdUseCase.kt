package com.krishnanepal.moneymanager.domain.use_case

import com.krishnanepal.moneymanager.data.repo.IncomeExpensesRepo
import javax.inject.Inject

class DeleteIncomeExpensesByIdUseCase@Inject constructor(
    private val repo: IncomeExpensesRepo
){
    suspend operator fun invoke(id:Int) {
        repo.deleteIncomeExpensesEntityById(id)
    }
}