package com.krishnanepal.moneymanager.utils

class Constants {

    object Strings {
        const val LOGOUT = "Logout"
        const val OK = "Ok"
        const val CANCEL = "Cancel"

        const val SUCCESS = "Success"
        const val INFO = "Info"
        const val ERROR = "Error"

        const val APP_NAME = "MoneyManager"

    }

    object Integers{

    }

    object Urls{

    }
}