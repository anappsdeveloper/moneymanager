package com.krishnanepal.moneymanager.utils

import android.content.Context
import android.content.SharedPreferences
import com.krishnanepal.moneymanager.utils.Constants.Strings.APP_NAME

class SessionManagement(val context: Context) {

    init {
        pref = context.getSharedPreferences(APP_NAME, Context.MODE_PRIVATE)
    }
    companion object{
        var pref: SharedPreferences? = null
        private const val SELECTED_DATE = "selected_date"
    }

    fun getSelectedDate(): String? {
        return pref?.getString(SELECTED_DATE, "")
    }

    fun setSelectedDate(date: String) {
        pref?.edit()?.putString(SELECTED_DATE, date)?.apply()
    }
}