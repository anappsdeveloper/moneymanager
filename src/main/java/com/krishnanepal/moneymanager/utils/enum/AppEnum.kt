package com.krishnanepal.moneymanager.utils.enum

enum class FragmentEnum(val str:String){
    MAIN_FRAGMENT("MainFragment"),
    GRAPH_FRAGMENT("GraphFragment")
}