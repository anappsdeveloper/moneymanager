package com.krishnanepal.moneymanager.utils.view_utils

import android.app.Activity
import android.content.Context
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.krishnanepal.moneymanager.R

class CustomToast(private val context: Context) {
    private var backColor = 0
    private var textColor = 0

    fun setBackground(backColor: Int) {
        this.backColor = backColor
    }

    fun setTextColor(textColor: Int) {
        this.textColor = textColor
    }

    fun showErrorToast(text: String?) {
        if (context != null) {
            val inflater = (context as Activity).layoutInflater
            val toastRoot: View = inflater.inflate(R.layout.layout_custom_toast, null)
            val linearLayout = toastRoot.findViewById<LinearLayout>(R.id.whole_layout)
            linearLayout.setBackgroundColor(context.getResources().getColor(R.color.design_default_color_error))
            val toastText = toastRoot.findViewById<TextView>(R.id.toastTitle)
            val toastImage = toastRoot.findViewById<ImageView>(R.id.toastImg)
            val toastSuccessTv = toastRoot.findViewById<TextView>(R.id.toastDesc)

            toastText.text = context.getResources().getString(R.string.error)
            toastSuccessTv.text = text
            Glide.with(context).load(R.drawable.ic_error).into(toastImage)

            val toast = Toast(context)
            toast.setView(toastRoot)
            toast.setGravity(
                Gravity.BOTTOM or Gravity.FILL_HORIZONTAL,
                0, 0
            )
            toast.duration = Toast.LENGTH_SHORT
            toast.show()
        }
    }

    fun showSuccessToast(text: String?) {
        if (context != null) {
            val inflater = (context as Activity).layoutInflater
            val toastRoot: View = inflater.inflate(R.layout.layout_custom_toast, null)
            val linearLayout = toastRoot.findViewById<View>(R.id.whole_layout) as LinearLayout
            linearLayout.setBackgroundColor(context.getResources().getColor(R.color.color_green_1))
            val toastText = toastRoot.findViewById<View>(R.id.toastTitle) as TextView
            val toastImage = toastRoot.findViewById<View>(R.id.toastImg) as ImageView
            val toastSuccessTv = toastRoot.findViewById<View>(R.id.toastDesc) as TextView

            toastText.text = "Success"
            toastSuccessTv.text = text
            toastImage.setImageResource(R.drawable.ic_success)

            val toast = Toast(context)
            toast.setView(toastRoot)
            toast.setGravity(
                Gravity.BOTTOM or Gravity.FILL_HORIZONTAL,
                0, 0
            )
            toast.duration = Toast.LENGTH_SHORT
            toast.show()
        }
    }

    fun showInfoToast(text: String?) {
        if (context != null) {
            val inflater = (context as Activity).layoutInflater
            val toastRoot: View = inflater.inflate(R.layout.layout_custom_toast, null)
            val linearLayout = toastRoot.findViewById<LinearLayout>(R.id.whole_layout)
            linearLayout.setBackgroundColor(
                context.getResources().getColor(R.color.color_text_blue_1)
            )
            val toastText = toastRoot.findViewById<TextView>(R.id.toastTitle)
            val toastImage = toastRoot.findViewById<ImageView>(R.id.toastImg)
            val toastSuccessTv = toastRoot.findViewById<TextView>(R.id.toastDesc)

            toastImage.setImageResource(R.drawable.ic_info)
            toastText.text = "Attention"
            toastSuccessTv.text = text

            val toast = Toast(context)
            toast.setView(toastRoot)
            //            toast.setGravity(Gravity.TOP | Gravity.FILL_HORIZONTAL, 0, 0);
            toast.setGravity(
                Gravity.BOTTOM or Gravity.FILL_HORIZONTAL,
                0, 0
            )
            toast.duration = Toast.LENGTH_SHORT
            toast.show()
        }
    }
}