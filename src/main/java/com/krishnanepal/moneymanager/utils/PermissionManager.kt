/*
package com.krishnanepal.moneymanager.utils

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.krishnanepal.moneymanager.common.Constants

class PermissionManager(var context: Activity) {

    fun hasCameraPermission() =
       ActivityCompat.checkSelfPermission(context, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED

    fun requestPermission(){
        var permissionToRequest = mutableListOf<String>()
        if(!hasCameraPermission()){
            permissionToRequest.add(Manifest.permission.CAMERA)
        }

        if(permissionToRequest.isNotEmpty()){
            //request for permission
            ActivityCompat.requestPermissions(context,permissionToRequest.toTypedArray(),Constants.Integers.IMAGE_CAPTURE_CODE)
        }
    }
}
*/
