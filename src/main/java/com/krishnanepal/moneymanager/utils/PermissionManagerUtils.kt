package com.krishnanepal.moneymanager.utils

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.krishnanepal.moneymanager.common.Constants

class PermissionManagerUtils(var context: Context) {
    var permissions = ArrayList<String>()

    init {
        permissions.add(Manifest.permission.CAMERA)
        permissions.add(Manifest.permission.RECORD_AUDIO)
       /* permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE)
        permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)*/
    }

    fun requestPermissions() {
        val listPermissionsNeeded = hasPermisssion()
       /* val listPermissionsNeeded: MutableList<String> = ArrayList()

        for (permission in permissions) {
            if (ContextCompat.checkSelfPermission(
                    context,
                    permission
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                listPermissionsNeeded.add(permission)
            }
        }*/

        if (listPermissionsNeeded.isNotEmpty()) {
            ActivityCompat.requestPermissions(
                context as Activity,
                listPermissionsNeeded.toTypedArray(),
                Constants.Integers.ALL_PERMISSIONS
            )
        }
    }

    fun isAllPermissionGranted(): Boolean{
        val listPermissionsNeeded = hasPermisssion()
//        val listPermissionsNeeded: MutableList<String> = ArrayList()
//
//        for (permission in permissions) {
//            if (ContextCompat.checkSelfPermission(
//                    context,
//                    permission
//                ) != PackageManager.PERMISSION_GRANTED
//            ) {
//                listPermissionsNeeded.add(permission)
//            }
//        }

        return listPermissionsNeeded.size <= 0
    }

  private fun  hasPermisssion(): MutableList<String>{
      val listPermissionsNeeded: MutableList<String> = ArrayList()
      for (permission in permissions) {
          if (ContextCompat.checkSelfPermission(
                  context,
                  permission
              ) != PackageManager.PERMISSION_GRANTED
          ) {
              listPermissionsNeeded.add(permission)
          }
      }
      return listPermissionsNeeded
  }
}