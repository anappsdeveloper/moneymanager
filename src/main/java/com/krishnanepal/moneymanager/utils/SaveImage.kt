package com.krishnanepal.moneymanager.utils

import android.content.ContentValues
import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import com.krishnanepal.moneymanager.utils.extentions.showErrorToast
import java.io.BufferedOutputStream
import java.io.File
import java.io.OutputStream

class SaveImage(var context: Context) {
    public fun saveImageToGallery(fileName: String/*, bitmap: Bitmap*/) : Boolean {
        var saveSuccess = false
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {

                val fos: OutputStream
                val resolver = context.contentResolver
                val values = ContentValues()

                values.put(MediaStore.MediaColumns.DISPLAY_NAME, fileName)
                values.put(MediaStore.MediaColumns.MIME_TYPE, "image/*")
                values.put(
                    MediaStore.MediaColumns.RELATIVE_PATH,
                    Environment.DIRECTORY_PICTURES + File.separator + "FolderName"
                )

                val imageUri = resolver.insert(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    values
                )
                fos = resolver.openOutputStream(imageUri!!) as OutputStream
//                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos)
                fos.close()

//                context.showSuccessToast("Image Has Been Saved Successfully.")

                saveSuccess = true
            } else {
                val values = ContentValues()
                values.put(MediaStore.Images.Media.MIME_TYPE, "image/*")
                values.put(MediaStore.Images.ImageColumns.DISPLAY_NAME, fileName)
                values.put(MediaStore.Images.ImageColumns.TITLE, fileName)

                val uri: Uri? = context.contentResolver.insert(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    values
                )
                uri?.let {
                    context.contentResolver.openOutputStream(uri)?.let { stream ->
                        val oStream =
                            BufferedOutputStream(stream)
//                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, oStream)
                        oStream.close()
//                        context.showSuccessToast("Image Has Been Saved Successfully.")

                    }
                }
                saveSuccess = true
            }
        } catch (e: Exception) {
            context.showErrorToast( e.localizedMessage)
            saveSuccess = false
        }
        return saveSuccess
    }
}