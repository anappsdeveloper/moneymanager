package com.krishnanepal.moneymanager.utils.constracts

class AppContract {

    object String{
        const val MAIN_FRAGMENT = "MainFragment"
        const val GRAPH_FRAGMENT = "GraphFragment"
        const val DATE_FORMAT = "dd-MM-yyyy"
    }
}