package com.krishnanepal.moneymanager.utils

import android.content.ContentResolver
import android.content.Context
import android.net.Uri
import android.provider.MediaStore
import android.provider.OpenableColumns
import android.util.Log
import android.webkit.MimeTypeMap

object FileManagerUtils {
    fun getName(uri: Uri?, context: Context): String {
        var fileName = ""
        val cr = context.contentResolver
        val projection = arrayOf(MediaStore.MediaColumns.DISPLAY_NAME)
        val metaCursor = cr.query(uri!!, projection, null, null, null)
        if (metaCursor != null) {
            try {
                if (metaCursor.moveToFirst()) {
                    fileName = metaCursor.getString(0)
                }
            } finally {
                metaCursor.close()
            }
        }
        return fileName
    }

    fun getSize(uri: Uri?, context: Context): Long {
        var fileSizeInMB: Long = 0
        val cr = context.contentResolver
        val projection = arrayOf(MediaStore.MediaColumns.SIZE)
        val metaCursor = cr.query(uri!!, projection, null, null, null)
        if (metaCursor != null) {
            try {
                if (metaCursor.moveToFirst()) {
                    val fileName = metaCursor.getString(0)
                    val size = metaCursor.getLong(metaCursor.getColumnIndex(OpenableColumns.SIZE))
                    Log.e("size_here", size.toString())
                    val fileSizeInKB = size / 1024
                    // Convert the KB to MegaBytes (1 MB = 1024 KBytes)
                    fileSizeInMB = fileSizeInKB / 1024
                    Log.e("fileSizeInKB", fileSizeInKB.toString())
                    Log.e("fileSizeInMB", fileSizeInMB.toString())
                }
            } finally {
                metaCursor.close()
            }
        }
        return fileSizeInMB
    }

    fun getMimeType(uri: Uri?, context: Context): String? {
        var mimeType: String? = null
        mimeType = if (uri?.scheme == ContentResolver.SCHEME_CONTENT) {
            val cr = context.contentResolver
            cr.getType(uri)
        } else {
            val fileExtension = MimeTypeMap.getFileExtensionFromUrl(
                uri
                    .toString()
            )
            MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                fileExtension.toLowerCase()
            )
        }
        return mimeType
    }
}