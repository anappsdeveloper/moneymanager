package com.krishnanepal.moneymanager.utils.extentions

import android.content.Context
import com.krishnanepal.moneymanager.ui.home.HomeAdapter
import com.krishnanepal.moneymanager.utils.constracts.AppContract
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

fun convertStringDateToDateObject(strDate: String): Date {
    return SimpleDateFormat(AppContract.String.DATE_FORMAT).parse(strDate)
}

fun convertDateToString(date: Date):String{
    val dateFormat: DateFormat = SimpleDateFormat("dd-MM-yyyy")
    return dateFormat.format(date)
}