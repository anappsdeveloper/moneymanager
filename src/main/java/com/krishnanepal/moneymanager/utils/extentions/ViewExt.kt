package com.krishnanepal.moneymanager.utils.extentions

import android.content.Context
import android.graphics.Color
import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.res.ResourcesCompat
import com.google.android.material.internal.ToolbarUtils.getTitleTextView
import com.krishnanepal.moneymanager.R
import com.krishnanepal.moneymanager.utils.Constants
import com.krishnanepal.moneymanager.utils.view_utils.CustomToast

fun Context.showAlertDialog(message: String?) {
    val builder: AlertDialog = AlertDialog.Builder(this)
        .setCustomTitle(this.getTitleTextView())
        .setView(R.layout.layout_alertdialog)
        .create()
    builder.show()
    val diagButton =
        builder.findViewById<View>(R.id.btn_ok) as Button?
    builder.findViewById<TextView>(R.id.text_message)?.text = message
    diagButton!!.setOnClickListener {
        builder.dismiss()
    }
}

fun Context.showAlertDialog(message: String?, positiveButton: String, positiveAction: () -> Unit) {
    val builder: AlertDialog = AlertDialog.Builder(this)
        .setCustomTitle(this.getTitleTextView())
        .setView(R.layout.layout_alertdialog)
        .create()
    builder.show()
    val diagButton =
        builder.findViewById<View>(R.id.btn_ok) as Button?
    diagButton?.text = positiveButton
    builder.findViewById<TextView>(R.id.text_message)?.text = message
    diagButton!!.setOnClickListener {
        builder.dismiss()
        positiveAction()
    }
}

    fun Context.showAlertDialog(
        message: String,
        positiveButton: String = Constants.Strings.OK,
        negativeButton: String = Constants.Strings.CANCEL,
        negativeAction: () -> Unit = {},
        positiveAction: () -> Unit,
        isCancelable: Boolean = false
    ) {
        val builder: AlertDialog = AlertDialog.Builder(this)
            .setCustomTitle(this.getTitleTextView())
            .setView(R.layout.layout_show_alert_with_buttons)
            .setCancelable(isCancelable)
            .create()
        builder.show()
        val positiveBtn =
            builder.findViewById<View>(R.id.btn_ok) as Button?
        positiveBtn?.text = positiveButton
        positiveBtn!!.setOnClickListener {
            builder.dismiss()
            positiveAction()
        }
        val negativeBtn =
            builder.findViewById<View>(R.id.btn_cancel) as Button?
        negativeBtn?.text = negativeButton
        negativeBtn!!.setOnClickListener {
            builder.dismiss()
            negativeAction()
        }
        builder.findViewById<TextView>(R.id.text_message)?.text = message
    }

    fun Context.getTitleTextView(): TextView {
        val title = TextView(this)
        title.text = getString(R.string.app_name)
        title.gravity = Gravity.CENTER
        title.setTextColor(Color.BLACK)
        title.setPadding(0, 30, 0, 0)
        val typeface = ResourcesCompat.getFont(this, R.font.lato)
        title.typeface = typeface
        title.textSize = 18f
        return title
    }

    fun Context.showToast(message: String?) {
        Toast.makeText(this, message ?: "", Toast.LENGTH_SHORT).show()
    }
