package com.krishnanepal.moneymanager.utils.extentions

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog.Builder
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.krishnanepal.moneymanager.R
import com.krishnanepal.moneymanager.common.Constants
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream

fun Context.showSuccessToast(message: String?) {
    val layout = LayoutInflater.from(this)
        .inflate(R.layout.custom_toast, null)
    val toast = Toast(this)

    val container = layout.findViewById<ConstraintLayout>(R.id.container)
    val tvMessage = layout.findViewById<TextView>(R.id.tvMessage)
    val ivIcon = layout.findViewById<ImageView>(R.id.ivIcon)
    ivIcon.setImageResource(R.drawable.ic_baseline_check_box_24)
    container.background =
        ContextCompat.getDrawable(this, R.drawable.rounded_background_green)
    tvMessage.text = message
    toast.duration = Toast.LENGTH_SHORT
    toast.view = layout //setting the view of custom toast layout
    toast.show()
}

fun Context.showWarningToast(message: String?) {
    val layout = LayoutInflater.from(this)
        .inflate(R.layout.custom_toast, null)
    val toast = Toast(this)
    val container = layout.findViewById<ConstraintLayout>(R.id.container)
    val tvMessage = layout.findViewById<TextView>(R.id.tvMessage)
    val ivIcon = layout.findViewById<ImageView>(R.id.ivIcon)
    ivIcon.setImageResource(R.drawable.ic_baseline_info_24)
    container.background =
        ContextCompat.getDrawable(this, R.drawable.round_background_orange)
    tvMessage.text = message
    toast.duration = Toast.LENGTH_SHORT
    toast.view = layout //setting the view of custom toast layout
    toast.show()
}

fun Context.showErrorToast(message: String?) {
    val layout = LayoutInflater.from(applicationContext).inflate(R.layout.custom_toast, null)
    val toast = Toast(applicationContext)
    val container = layout.findViewById<ConstraintLayout>(R.id.container)
    val tvMessage = layout.findViewById<TextView>(R.id.tvMessage)
    val ivIcon = layout.findViewById<ImageView>(R.id.ivIcon)
    ivIcon.setImageResource(R.drawable.ic_baseline_error_24)
    container.background = ContextCompat.getDrawable(this, R.drawable.rounded_background_red)
    tvMessage.text = message
    toast.duration = Toast.LENGTH_SHORT
    toast.view = layout
    toast.show()
}

fun Context.showOkCancelMessage(string: String, action: () -> Unit) {
    android.app.AlertDialog.Builder(this).apply {
        setMessage(string)
            .setPositiveButton("Ok") { dialog, _ ->
                dialog.dismiss()
                action()
            }
        setNegativeButton("Cancel", { dialog, _ ->
            dialog.dismiss()
        })
        create()
        show()

    }
}


fun Context.showDialogMessage(string: String) {
    android.app.AlertDialog.Builder(this).apply {
        setMessage(string)
        setPositiveButton("Ok", null)
        create()
        show()

    }
}

fun Context.getBitmap(context: Context, imageUri: Uri): Bitmap? {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {

        ImageDecoder.decodeBitmap(
            ImageDecoder.createSource(
                context.contentResolver,
                imageUri
            )
        )

    } else {

        context
            .contentResolver
            .openInputStream(imageUri)?.use { inputStream ->
                BitmapFactory.decodeStream(inputStream)
            }

    }
}

fun Context.convertBitmapToFile(destinationFile: File, bitmap: Bitmap) {
    //create a file to write bitmap data
    destinationFile.createNewFile()
    //Convert bitmap to byte array
    val bos = ByteArrayOutputStream()
    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bos)
    val bitmapData = bos.toByteArray()
    //write the bytes in file
    val fos = FileOutputStream(destinationFile)
    fos.write(bitmapData)
    fos.flush()
    fos.close()
}


fun Context.showConfirmationDialog(
    title: String,
    message: String,
    positiveButton: String = Constants.Strings.OK,
    negativeButton: String = Constants.Strings.CANCEL,
    positiveAction: () -> Unit,
    negativeAction: () -> Unit = {}
) {
    val builder =
        AlertDialog.Builder(this)
    builder.setTitle(title)
        .setMessage(message)
        .setPositiveButton(positiveButton) { dialog, _ ->
            dialog.dismiss()
            positiveAction()
        }
        .setNegativeButton(negativeButton) { dialog, _ ->
            dialog.dismiss()
            negativeAction()
        }

    val alertDialog = builder.create()
    alertDialog.show()
}