package com.krishnanepal.moneymanager.utils.extentions

import com.krishnanepal.moneymanager.common.Constants
import java.util.regex.Pattern

fun String.getSectionHeader(): Char {
    val firstLetter = get(0).uppercaseChar()
    val pattern = Pattern.compile("[^A-Za-z]")
    val matcher = pattern.matcher(firstLetter.toString())
    return if (matcher.find()) {
        '#'
    } else {
        firstLetter
    }
}

/**
 * Email validation .
 */
fun String.isValidEmail(): Boolean {

    val pattern = Pattern.compile(
        "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                "\\@" +
                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                "(" +
                "\\." +
                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                ")+"
    )
    return pattern.matcher(this).matches()
}


//fun String.isValidPhone(): Boolean {
//    if (!TextUtils.isEmpty(this)) {
//        return Patterns.PHONE.matcher(this).matches()
//    }
//    return false
//}

fun String.isValidPhone(): Boolean {
    val pattern = Pattern.compile(
        "^(?:\\+?(61))? ?(?:\\((?=.*\\)))?(0?[2-57-8])\\)? ?(\\d\\d(?:[- ](?=\\d{3})|(?!\\d\\d[- ]?\\d[- ]))\\d\\d[- ]?\\d[- ]?\\d{3})\$"
    )
    return pattern.matcher(this).matches()
}

/**
 * validate password with at least 8 character, One upper case , one lower case and digit regex
 *
 * @param passwordTxt => user password text
 */
fun String.isValidPassword(): Boolean {

    val pattern = Pattern.compile("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#\$%^&+=])(?=\\S+\$).{4,}\$")
    return pattern.matcher(this).matches()
//    return this.length >= 6
////        return false
}

/**
 * validate empty field
 *
 * @param value => value from edittext
 */
fun String.isTextEmpty(): Boolean {
    return this.trim().isEmpty()
}

/**
 * check email is already used or not
 ** */
fun String.isEmailAlreadyUsed(): Boolean {
    return this == "tr1@getnada.com"
}

fun String.isFirstNameValid(): Boolean {
    return this.length > 2
}

fun String.isSuccess(): Boolean {
    val pattern = Constants.Strings.SUCCESS
    return pattern.equals(this)
}