package com.krishnanepal.moneymanager.utils

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.content.Context
import android.widget.EditText
import com.krishnanepal.moneymanager.utils.constracts.AppContract
import java.text.SimpleDateFormat
import java.util.*

class DatePickerUtils(context: Context, editText: EditText?,
                      private val datePickerInterface: DatePickerInterface,
                     private val isFilter:Boolean) {

    private val calendar = Calendar.getInstance()
    private val dateFormat = AppContract.String.DATE_FORMAT
    private var editText:EditText?=null

    var sessionManagement: SessionManagement

    private val onDateSetChangeListener: OnDateSetListener
    private get() = OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        calendar[Calendar.YEAR] = year
        calendar[Calendar.MONTH] = monthOfYear
        calendar[Calendar.DAY_OF_MONTH] = dayOfMonth

        val sdf = SimpleDateFormat(dateFormat,Locale.US)
        val selectedDate = sdf.format(calendar.time)
        editText?.setText(selectedDate)

        datePickerInterface.setFilterBySelectedDate(selectedDate,isFilter)
        sessionManagement.setSelectedDate(sdf.format(calendar.time))

    }

     fun getDatePickerDialog(context: Context):DatePickerDialog{

        val datePickerDialog = DatePickerDialog(context,
            AlertDialog.THEME_HOLO_LIGHT,
          /* R.style.my_dialog_theme,*/
            onDateSetChangeListener,
            calendar[Calendar.YEAR],
            calendar[Calendar.MONTH],
            calendar[Calendar.DAY_OF_MONTH]
        )
        datePickerDialog.datePicker.maxDate = System.currentTimeMillis()
        datePickerDialog.datePicker.spinnersShown = true
        datePickerDialog.datePicker.calendarViewShown = false
        return datePickerDialog
    }

    init {
        this.editText = editText
        getDatePickerDialog(context).show()
        sessionManagement = SessionManagement(context)
    }

    interface DatePickerInterface{
        fun setFilterBySelectedDate(date:String, isFilter: Boolean)
    }
}