package com.krishnanepal.moneymanager.ui.media

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.provider.MediaStore
import android.util.Log
import android.widget.MediaController
import android.widget.SeekBar
import android.widget.SeekBar.OnSeekBarChangeListener
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import com.krishnanepal.moneymanager.common.Constants
import com.krishnanepal.moneymanager.databinding.ActivityCameraBinding
import com.krishnanepal.moneymanager.utils.FileManagerUtils
import com.krishnanepal.moneymanager.utils.FileUtils
import com.krishnanepal.moneymanager.utils.PermissionManagerUtils
import com.krishnanepal.moneymanager.utils.extentions.showConfirmationDialog
import java.io.File
import java.io.FileNotFoundException
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class CameraActivity : AppCompatActivity() {
    private lateinit var binding: ActivityCameraBinding
    lateinit var currentPhotoPath: String

     private var mediaPlayer: MediaPlayer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCameraBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

            PermissionManagerUtils(this@CameraActivity).requestPermissions()


        binding.btnCamera.setOnClickListener {
            if (PermissionManagerUtils(this@CameraActivity).isAllPermissionGranted()) {
                captureImage()
//                dispatchTakePictureIntent()
            } else {

                showConfirmationDialog(
                    "Permission Missing",
                    "Permission is Required For Application To Function Correctly. Please Grant All The Permissions.",
                    "Ok",
                    "Cancel",
                    {
                        PermissionManagerUtils(this@CameraActivity).requestPermissions()
                    },
                    {}
                )
            }
        }

        binding.btnVideoCamera.setOnClickListener {
            captureVideo()
        }

        binding.btnAudio.setOnClickListener {
            captureAudio()
        }

    }

    private fun captureVideo() {
        val takeVideoIntent = Intent(MediaStore.ACTION_VIDEO_CAPTURE)
        if (takeVideoIntent.resolveActivity(packageManager) != null) {
            startActivityForResult(takeVideoIntent, Constants.Integers.VIDEO_CAPTURE_CODE)
        }
    }

    private fun captureAudio() {
        val intent = Intent(MediaStore.Audio.Media.RECORD_SOUND_ACTION)
        startActivityForResult(intent, Constants.Integers.AUDIO_CAPTURE_CODE)
    }

    private fun dispatchTakePictureIntent() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        try {
            startActivityForResult(takePictureIntent, Constants.Integers.IMAGE_CAPTURE_CODE)
        } catch (e: ActivityNotFoundException) {
            // display error state to the user
        }
    }


    private fun captureImage() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(packageManager)?.also {
                val photoFile: File? = try {
                    createImageFile()
                } catch (ex: IOException) {
                    null
                }
                photoFile?.also {
                    val photoURI: Uri = FileProvider.getUriForFile(
                        this,
                        "com.example.android.fileprovider",
                        it
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent, Constants.Integers.IMAGE_CAPTURE_CODE)
                }
            }
        }

    /*    val builder = VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())

        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        intent.putExtra(
            MediaStore.EXTRA_OUTPUT,
            Uri.fromFile(getTempFile(this))
        )
        startActivityForResult(
            intent,
            123
        )*/
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File? = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            currentPhotoPath = absolutePath
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            when(requestCode){
                Constants.Integers.IMAGE_CAPTURE_CODE -> {
                    binding.iv.setImageURI(Uri.parse(currentPhotoPath))
//                      val imageBitmap = data?.extras?.get("data") as Bitmap
//                      binding.iv.setImageBitmap(imageBitmap)

//                    SaveImage(this).saveImageToGallery(currentPhotoPath)
                }
                Constants.Integers.VIDEO_CAPTURE_CODE -> {
                    val fileUrl = data?.data
                    binding.videoView.setVideoURI(fileUrl)
                    binding.videoView.start()

                    val mediaController = MediaController(this)
                    binding.videoView.setMediaController(mediaController)
                    mediaController.setAnchorView(binding.frame)
                    if (fileUrl !== null) {
                        val file = FileUtils.from(this, fileUrl)
                        Log.d("filenameufdsfsd",file.path)
                    }
                }

                Constants.Integers.AUDIO_CAPTURE_CODE -> {
                    val fileUri = data?.data
                    try {
                        val file: File = FileUtils.from(this, fileUri)
                        val mimeType = FileManagerUtils.getMimeType(fileUri, this)
                        Log.d("qwerty",file.path)

                        //////////
                        binding.btnPlay.setOnClickListener {
                            if(mediaPlayer == null){
                                mediaPlayer = MediaPlayer.create(this,fileUri)
                                mediaPlayer?.setOnCompletionListener(MediaPlayer.OnCompletionListener {
                                   stopPlayer()
                                })
                            }
                            mediaPlayer?.start()

                           /* Handler().postDelayed({
                                if (mediaPlayer != null) {
                                    val mCurrentPosition: Int =
                                        mediaPlayer?.currentPosition?.div(1000) ?: 1000
                                    binding.seekBar.progress = mCurrentPosition
                                }
                            }, 1)*/
                            val mHandler = Handler()
                            this@CameraActivity.runOnUiThread(object : Runnable {
                                override fun run() {
                                    if (mediaPlayer != null) {
                                        val mCurrentPosition: Int =
                                            mediaPlayer?.currentPosition?.div(1000) ?: 1000
                                        binding.seekBar.progress = mCurrentPosition
                                    }
                                    mHandler.postDelayed(this, 1)
                                }
                            })

                            binding.seekBar.max = (mediaPlayer?.duration?.div(1000)) ?: 1000

                            binding.seekBar.setOnSeekBarChangeListener(object : OnSeekBarChangeListener {
                                override fun onStopTrackingTouch(seekBar: SeekBar) {}
                                override fun onStartTrackingTouch(seekBar: SeekBar) {}
                                override fun onProgressChanged(
                                    seekBar: SeekBar,
                                    progress: Int,
                                    fromUser: Boolean
                                ) {
                                    if (mediaPlayer != null && fromUser) {
                                        mediaPlayer?.seekTo(progress * 1000)
                                    }
                                }
                            })
                        }

                        binding.btnPause.setOnClickListener {
                            if(mediaPlayer != null){
                                mediaPlayer?.pause()
                            }
                        }

                        binding.btnStop.setOnClickListener {
                          stopPlayer()
                        }

                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }

            /*    123->{
                    var captureBmp: Bitmap? = null
                    val file = getTempFile(this)
                    try {
                        val uri = Uri.fromFile(file)
                        captureBmp = MediaStore.Images.Media.getBitmap(
                           this.getContentResolver(),
                            uri
                        )
                        binding.iv.setImageBitmap(captureBmp)
                    } catch (e: FileNotFoundException) {
                        e.printStackTrace()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }*/
            }
        }
    }

    private fun stopPlayer(){
        if(mediaPlayer != null){
            mediaPlayer?.release()
            mediaPlayer = null
        }
    }

    override fun onStop() {
        super.onStop()
        stopPlayer()
    }

    private fun getTempFile(context: Context): File? {
        val path = File(
            Environment.getExternalStorageDirectory(),
            context.packageName
        )
        if (!path.exists()) {
            path.mkdir()
        }
        return File(path, "myImage.png")
    }
    }