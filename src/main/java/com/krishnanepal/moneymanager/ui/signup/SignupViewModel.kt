package com.krishnanepal.moneymanager.ui.signup

import androidx.lifecycle.ViewModel
import com.krishnanepal.moneymanager.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class SignupViewModel
@Inject constructor() : BaseViewModel() {

}