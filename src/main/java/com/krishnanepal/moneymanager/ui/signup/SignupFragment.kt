package com.krishnanepal.moneymanager.ui.signup

import androidx.fragment.app.viewModels
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.krishnanepal.moneymanager.R
import com.krishnanepal.moneymanager.base.BaseFragment
import com.krishnanepal.moneymanager.databinding.FragmentLoginBinding
import com.krishnanepal.moneymanager.databinding.FragmentSignupBinding
import com.krishnanepal.moneymanager.ui.login.LoginViewModel
import com.krishnanepal.moneymanager.utils.Constants
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SignupFragment : BaseFragment<FragmentSignupBinding, SignupViewModel>() {


    private val signupViewModel: SignupViewModel by viewModels()
    private lateinit var auth: FirebaseAuth
    lateinit var userName:String
    lateinit var password:String

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        FirebaseApp.initializeApp(requireActivity())
        auth = FirebaseAuth.getInstance()
        binding.btnSignup.setOnClickListener {
            userName = binding.etUsername.text.toString()
            password = binding.etPassword.text.toString()
            if (userName.length > 6) {
//                systemPrefManager.save(SystemPreferenceManager.USER_NAME, userName)
//                startActivity(Intent(this@WelcomeActivity, MainActivity::class.java))
                hitFirebase(userName,password)
            }else{
                Toast.makeText(requireActivity(), "Please enter valid name", Toast.LENGTH_LONG).show()
                showToastMessage("Please enter valid name", Constants.Strings.INFO)
            }
        }


    }

    private fun hitFirebase(userName: String, password: String) {

        auth.createUserWithEmailAndPassword(userName, password)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d("SignupFragmentTag", "createUserWithEmail:success")
                    val user = auth.currentUser
                    findNavController().navigate(R.id.action_signupFragment_to_mainFragment)
                    showToastMessage("Authenticate succeed", Constants.Strings.SUCCESS)
//                    updateUI(user)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.d("SignupFragmentTag", "createUserWithEmail:failure", task.exception)
                    showToastMessage("Authenticate failed", Constants.Strings.ERROR)
//                    updateUI(null)
                }

            }
    }

    override fun onStart() {
        super.onStart()
        val currentUser = auth.currentUser
        if(currentUser!=null){
            Log.d("LoginFragmentTag", "currentuser")
        }
    }

    override fun getLayoutId() = R.layout.fragment_signup

    override fun getViewModel() = signupViewModel
}