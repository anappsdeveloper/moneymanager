package com.krishnanepal.moneymanager.ui.home

import android.annotation.SuppressLint
import android.content.ContentValues.TAG
import android.nfc.Tag
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.widget.Button
import android.widget.EditText
import android.widget.RadioGroup
import android.widget.TextView
import androidx.appcompat.widget.PopupMenu
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.viewModels
import androidx.lifecycle.*
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.krishnanepal.moneymanager.R
import com.krishnanepal.moneymanager.base.BaseFragment
import com.krishnanepal.moneymanager.common.CatIncomeEnum
import com.krishnanepal.moneymanager.common.CategoryEnum
import com.krishnanepal.moneymanager.common.Constants.Strings.SUCCESS
import com.krishnanepal.moneymanager.data.local.IncomeExpensesEntity
import com.krishnanepal.moneymanager.databinding.FragmentHomeBinding
import com.krishnanepal.moneymanager.utils.Constants
import com.krishnanepal.moneymanager.utils.Constants.Strings.INFO
import com.krishnanepal.moneymanager.utils.DatePickerUtils
import com.krishnanepal.moneymanager.utils.SessionManagement
import com.krishnanepal.moneymanager.utils.extentions.convertStringDateToDateObject
import com.krishnanepal.moneymanager.utils.extentions.showToast
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import java.util.*
import javax.inject.Inject
import javax.inject.Named

@AndroidEntryPoint
class HomeFragment : BaseFragment<FragmentHomeBinding, HomeViewModel>(),HomeAdapter.HomeAdapterInterface, DatePickerUtils.DatePickerInterface,
    ShowDetailAdapter.ShowDetailInterface/*,SearchView.OnQueryTextListener */{

    private val homeViewModel: HomeViewModel by viewModels()
    lateinit var user:FirebaseUser
    lateinit var auth:FirebaseAuth

    lateinit var homeAdapter:HomeAdapter
    lateinit var showDetailAdapter: ShowDetailAdapter

    private var incomeExpensesList = mutableListOf<IncomeExpensesEntity>()

    lateinit var sessionManagement: SessionManagement

    lateinit var updateList:MutableList<IncomeExpensesEntity>

//     var selectedDate:String = ""

    @Inject
    @Named("error")
    lateinit var errorMessage: String

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        FirebaseApp.initializeApp(requireActivity())
        auth = FirebaseAuth.getInstance()
        user = auth.currentUser!!
        if(user == null){
            findNavController().navigate(R.id.action_mainFragment_to_loginFragment)
        }else{
            user.email?.let {
              showToastMessage("Welcome to app  ${user.email}",INFO)
            }
        }


        initUi()
        initVM()
    }

    private fun initVM() {
        lifecycle.coroutineScope.launchWhenStarted {
            homeViewModel.dailyIncomeExpensesState.collect {
                if(it.isNullOrEmpty()){
                binding.layoutDataNotFound.root.visibility = View.VISIBLE
                    binding.root.visibility = View.GONE
                }else{
                    binding.layoutDataNotFound.root.visibility = View.GONE
                    binding.root.visibility = View.VISIBLE
                    homeAdapter = HomeAdapter(it, this@HomeFragment,requireActivity())
                    binding.rv.adapter = homeAdapter
                }
            }
        }

      /*  lifecycle.coroutineScope.launchWhenStarted {
            homeViewModel.incomeExpensesState.collect {
                if(it.isNullOrEmpty()){
                    binding.layoutDataNotFound.root.visibility = View.VISIBLE
                    binding.root.visibility = View.GONE
                }else{
                    Log.d("fdsfdfd", Gson().toJson(it))
                    binding.layoutDataNotFound.root.visibility = View.GONE
                    binding.root.visibility = View.VISIBLE
                    showDetailAdapter = ShowDetailAdapter(this@HomeFragment,it)
                    binding.rv.adapter = showDetailAdapter
                }
            }
        }*/
    }

    private fun initUi() {

        sessionManagement = SessionManagement(requireContext())
        with(binding) {


            fab.setOnClickListener {
                addUpdateBottomSheetDialog(true,
                    null
                )
            }

            swiperefresh.setOnRefreshListener {
                homeViewModel.getDailyIncomeExpenses()
                swiperefresh.isRefreshing = false
            }
        }
    }

    private fun addUpdateBottomSheetDialog(
        isAddMode: Boolean,
        incomeExpensesEntity: IncomeExpensesEntity?
    ) {
        val bottomSheetDialog = BottomSheetDialog(requireContext())
        bottomSheetDialog.setContentView(R.layout.add_update_bottomsheet)

        val cg = bottomSheetDialog.findViewById<ChipGroup>(R.id.cg)
        val rg = bottomSheetDialog.findViewById<RadioGroup>(R.id.radioGroup)
        val etItem = bottomSheetDialog.findViewById<EditText>(R.id.et_item)
        val etPrice = bottomSheetDialog.findViewById<EditText>(R.id.et_price)
        val etDate = bottomSheetDialog.findViewById<EditText>(R.id.et_date)

        etDate?.setText(getCurrentDate())
        etDate?.setOnClickListener {
            DatePickerUtils(requireActivity(), etDate, this,false)
//            etDate.setText(selectedDate)
        }

        /**Chip*/
        var expensesType = true
        var selectedChip = if(incomeExpensesEntity?.isExpenses == true) CategoryEnum.Other.name else CatIncomeEnum.Other.name

        /**Update Mode*/
        if(!isAddMode) {
            if(incomeExpensesEntity?.isExpenses == true){
                expensesType = true
                rg?.check(R.id.rb_expenses)
            }else{
                expensesType = false
                rg?.check(R.id.rb_income)
            }
        }

        for (cat in if (expensesType) CategoryEnum.values() else CatIncomeEnum.values()) {
            val chip = this.layoutInflater.inflate(R.layout.item_chip, null, false) as Chip

            chip.text = cat.name
            cg?.addView(chip)

            /** Update chip select*/
            if(!isAddMode){
                if(cat.name == incomeExpensesEntity?.cat){
                    chip.isChecked = true
                    selectedChip = chip.text.toString()
                }
                /** Add default chip select*/
            }else{
                if(cat.name == CategoryEnum.Other.name || cat.name == CatIncomeEnum.Other.name)
                chip.isChecked = true
            }

            cg?.setOnCheckedChangeListener { chipGroup, id ->
                val chip1: Chip = cg.findViewById(id)
                selectedChip = chip1.text.toString()
            }
        }

        /**Radio*/
        rg?.setOnCheckedChangeListener { radioGroup, id ->
            cg?.removeAllViews()
            val checkedRadioButtonId = rg?.checkedRadioButtonId
            expensesType = checkedRadioButtonId == R.id.rb_expenses

            for (cat in if (expensesType) CategoryEnum.values() else CatIncomeEnum.values()) {
                val chip = this.layoutInflater.inflate(R.layout.item_chip, null, false) as Chip

                chip.text = cat.name
                cg?.addView(chip)

                cg?.setOnCheckedChangeListener { chipGroup, id ->
                    val chip1: Chip = cg.findViewById(id)
                    selectedChip = chip1.text.toString()
                }

                /**Update Mode*/
                if(!isAddMode){
                    if(cat.name == incomeExpensesEntity?.cat){
                        chip.isChecked = true
                        selectedChip = chip.text.toString()
                    }
                    /** Add Mode default chip select on radio btn click*/
                }else{
                    if(cat.name == CategoryEnum.Other.name || cat.name == CatIncomeEnum.Other.name)
                        chip.isChecked = true
                }

            }
        }

        /**Add or Update Button*/
        val btnAdd = bottomSheetDialog.findViewById<Button>(R.id.btn_add)
        val btnDelete = bottomSheetDialog.findViewById<Button>(R.id.btn_delete)
        btnAdd?.setOnClickListener {
            if (!homeViewModel.isItemEmpty(etItem)) {
            if(isAddMode) {
                homeViewModel.insertBothEntity(
                    IncomeExpensesEntity(
                        0,
                        etItem?.text.toString(),
                        homeViewModel.isPriceEmpty(etPrice),
                        selectedChip,
                        convertStringDateToDateObject(etDate?.text.toString()),
                        isExpenses = expensesType
                    )
                )
            }else{
                if (incomeExpensesEntity != null) {
                    homeViewModel.updateIncomeExpensesEntityById(
                        IncomeExpensesEntity(
                            incomeExpensesEntity.id,
                            etItem?.text.toString(),
                            homeViewModel.isPriceEmpty(etPrice),
                            selectedChip,
                            convertStringDateToDateObject(etDate?.text.toString()),
                            isExpenses = expensesType
                        )
                    )
                }
                //update query here
                Log.d("dsfdfds","update")
            }
                bottomSheetDialog.dismiss()
            }
        }

        /**Update Mode*/
        if(!isAddMode){
            btnAdd?.text = "Update"
            btnDelete?.visibility = View.VISIBLE
            etItem?.setText(incomeExpensesEntity?.item)
            etPrice?.setText(incomeExpensesEntity?.amt.toString())
          /*  etDate?.setText(incomeExpensesEntity?.date)*/
        }

        /**Delete Button*/
        btnDelete?.setOnClickListener {
        homeViewModel.deleteIncomeExpensesEntityByIdUseCase(incomeExpensesEntity?.id ?: -1)
        bottomSheetDialog.dismiss()
            homeViewModel.showSuccess("Delete succeed!")
        }
        bottomSheetDialog.show()
    }

    private fun showSearchBottomSheet(){
        val  bottomSheetDialog = BottomSheetDialog(requireContext())
        bottomSheetDialog.setContentView(R.layout.show_detail_bottomsheet)

        val rv = bottomSheetDialog.findViewById<RecyclerView>(R.id.rv)
        val etSearch = bottomSheetDialog.findViewById<EditText>(R.id.etSearch)
        homeViewModel.getIncomeExpenses()

        lifecycle.coroutineScope.launchWhenStarted {
            homeViewModel.incomeExpensesState.collect { list ->
                if(list.isNullOrEmpty()){
                    binding.layoutDataNotFound.root.visibility = View.VISIBLE
                    binding.root.visibility = View.GONE
                }else{
//                    Log.d("fdsfdfd", Gson().toJson(it))
                    binding.layoutDataNotFound.root.visibility = View.GONE
                    binding.root.visibility = View.VISIBLE
                    showDetailAdapter = ShowDetailAdapter(this@HomeFragment,list)
                    rv?.adapter = showDetailAdapter

                    if(list.size>=5){
                        etSearch?.visibility = View.VISIBLE
                        etSearch?.addTextChangedListener(object: TextWatcher{
                            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

                            }

                            override fun onTextChanged(s: CharSequence?, p1: Int, p2: Int, count: Int) {
                                if(count>0){
                                    var filterOption: MutableList<IncomeExpensesEntity> = mutableListOf()
                                    for(item in list){
                                        if(item.item.contains(s.toString().lowercase(), ignoreCase = true)){
                                            filterOption.add(item)
                                        }
                                    }
                                    showDetailAdapter.updateList(filterOption)
                                }else{
                                    showDetailAdapter.updateList(list)
                                }
                            }

                            override fun afterTextChanged(p0: Editable?) {

                            }

                        })
                    }else{
                        etSearch?.visibility = View.GONE
                    }
                }
            }
        }

        bottomSheetDialog.show()
    }


    private fun showDetailBottomSheet(date: Date) {
//        var list:MutableList<IncomeExpensesEntity> = mutableListOf()
       val  bottomSheetDialog = BottomSheetDialog(requireContext())
        bottomSheetDialog.setContentView(R.layout.show_detail_bottomsheet)

        val rv = bottomSheetDialog.findViewById<RecyclerView>(R.id.rv)
        val etSearch = bottomSheetDialog.findViewById<EditText>(R.id.etSearch)
        val tvTitle = bottomSheetDialog.findViewById<TextView>(R.id.tv_title)
        homeViewModel.getDailyIncomeExpensesByDate(date)
        lifecycle.coroutineScope.launchWhenStarted {
            homeViewModel.dailyIncomeExpensesByDateState.collect { list ->
//                list = it
                Log.d("dfdsfde3r",list.size.toString())
                showDetailAdapter = ShowDetailAdapter(this@HomeFragment,list)
                rv?.adapter = showDetailAdapter
                val dayAndMonth = date.toString().split(" ")
                tvTitle?.text = "Transaction made on: ${dayAndMonth[1] + " " + dayAndMonth[2]}"
                if(list.size>=5){
                    etSearch?.visibility = View.VISIBLE
                    etSearch?.addTextChangedListener(object: TextWatcher{
                        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

                        }

                        override fun onTextChanged(s: CharSequence?, p1: Int, p2: Int, count: Int) {
                            if(count>0){
                                var filterOption: MutableList<IncomeExpensesEntity> = mutableListOf()
                                for(item in list){
                                    if(item.item.contains(s.toString().lowercase(), ignoreCase = true)){
                                        filterOption.add(item)
                                    }
                                }
                                showDetailAdapter.updateList(filterOption)
                            }else{
                                showDetailAdapter.updateList(list)
                            }
                        }

                        override fun afterTextChanged(p0: Editable?) {

                        }

                    })
                }else{
                    etSearch?.visibility = View.GONE
                }

                if(!list.isNullOrEmpty())
                    swipeToDelete(/*showDetailAdapter.list,*/rv,bottomSheetDialog)
                    Log.d("fdsfd","hhe")

            }
        }

        bottomSheetDialog.show()
    }

    private fun showPopup(view: View, id: Int) {
        val popup = PopupMenu(requireContext(), view)
        popup.inflate(R.menu.list_menu)

        popup.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener {
            when (it.itemId) {
                R.id.m_edit -> {
                    popup.dismiss()
                }
                R.id.m_delete -> {
                    popup.dismiss()
                    homeViewModel.deleteIncomeExpensesEntityByIdUseCase(id)
                }
            }
            true
        })
        popup.show()
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.main_menu, menu);

        menu.findItem(R.id.filter).isVisible = true
        menu.findItem(R.id.sort).isVisible = true
        menu.findItem(R.id.search).isVisible = true

     /*   val search = menu?.findItem(R.id.search)
        val searchView = search?.actionView as? SearchView
        searchView?.isSubmitButtonEnabled = true
            searchView?.setOnQueryTextListener(this)

        searchView?.setOnSearchClickListener {
            showSearchBottomSheet()
        }*/
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return when (item.itemId) {
            R.id.logout -> {
                auth.signOut()
                findNavController().navigate(R.id.action_mainFragment_to_loginFragment)
                Log.d("fdfdffs","logout")
                requireActivity().showToast(Constants.Strings.LOGOUT)
                true
            }
            R.id.filter -> {
                DatePickerUtils(requireContext(),null,this@HomeFragment,true)

                true
            }
            R.id.sort ->{
            /*    Log.d("fsdfdfderere","sort")
                homeViewModel._dailyIncomeExpensesState.value.reverse()
                homeAdapter.notifyDataSetChanged()*/
                true
            }
            R.id.search -> {
                showSearchBottomSheet()
                Log.d(TAG,"fdsfdxxx")
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun swipeToDelete(
      /*  mutableList: MutableList<IncomeExpensesEntity>,*/
        rv: RecyclerView?,
        bottomSheetDialog: BottomSheetDialog
        ) {

        val swipeHandler = object : SwipeToDeleteCallback(requireContext()) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {

                addUpdateBottomSheetDialog(false,updateList[viewHolder.adapterPosition])
                bottomSheetDialog.dismiss()
            }
        }
        val itemTouchHelper = ItemTouchHelper(swipeHandler)
        itemTouchHelper.attachToRecyclerView(rv)
    }

    override fun getLayoutId() = R.layout.fragment_home

    override fun getViewModel():HomeViewModel = homeViewModel

    override fun onItemClick(date: Date) {
        showDetailBottomSheet(date)

//        requireActivity().showToast(errorMessage)
    }

    override fun setFilterBySelectedDate(selectedDate: String, isFilter: Boolean) {
       if(isFilter){
           Log.d("dfdfdf","terue")
        /* val listByDate = homeViewModel.dailyIncomeExpensesState.value.filter {  it.date == selectedDate }
           homeViewModel._dailyIncomeExpensesState.value = listByDate as MutableList<DailyModel>
*/
       }else{
//           this.selectedDate = selectedDate
           Log.d("dfdfdf","false")
           //came from edit
       }
    }

    override fun sendUpdateList(list: MutableList<IncomeExpensesEntity>) {
        this.updateList = list
    }

/*    override fun onQueryTextSubmit(query: String?): Boolean {
        Log.d("fdfdffs",query.toString())
      return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        Log.d("fdfdffs",newText.toString()+"///////")
        return true
    }*/
}