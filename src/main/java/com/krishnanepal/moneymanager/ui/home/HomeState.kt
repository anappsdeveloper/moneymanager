package com.krishnanepal.moneymanager.ui.home

import com.krishnanepal.moneymanager.data.local.IncomeExpensesEntity

data class HomeState(
    val isLoading: Boolean = false,
    val list:List<IncomeExpensesEntity> = emptyList(),
    val error:String = ""
)
