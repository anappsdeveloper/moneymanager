package com.krishnanepal.moneymanager.ui.home

import android.widget.EditText
import androidx.lifecycle.viewModelScope
import com.krishnanepal.moneymanager.base.BaseViewModel
import com.krishnanepal.moneymanager.data.local.IncomeExpensesEntity
import com.krishnanepal.moneymanager.data.model.DailyModel
import com.krishnanepal.moneymanager.domain.use_case.*
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

@HiltViewModel
class HomeViewModel
@Inject constructor(private val getIncomeExpensesUseCase: GetIncomeExpensesUseCase,
                    private val insertBothUseCase: InsertExpensesUseCase,
                    private val deleteIncomeExpensesByIdUseCase: DeleteIncomeExpensesByIdUseCase,
                    private val dailyIncomeExpensesUseCase: GetDailyIncomeExpensesUseCase,
                    private val incomeExpensesByDateUseCase: GetIncomeExpensesByDateUseCase,
                    private val updateUseCase: UpdateUseCase
) : BaseViewModel() {

    //all list
      val _incomeExpensesState = MutableStateFlow(mutableListOf<IncomeExpensesEntity>())
    val incomeExpensesState: StateFlow<MutableList<IncomeExpensesEntity>> = _incomeExpensesState

    //date wise both sum of income and expenses show
    val _dailyIncomeExpensesState = MutableStateFlow(mutableListOf<DailyModel>())
    val dailyIncomeExpensesState: StateFlow<MutableList<DailyModel>> = _dailyIncomeExpensesState

    //by specific datewise
    private val _incomeExpensesByDateState = MutableStateFlow(mutableListOf<IncomeExpensesEntity>())
    val dailyIncomeExpensesByDateState: StateFlow<MutableList<IncomeExpensesEntity>> = _incomeExpensesByDateState

    /*val _dailyIncomeExpensesByDateState = MutableStateFlow(mutableListOf<IncomeExpensesEntity>())
    val dailyIncomeExpensesByDateState: StateFlow<MutableList<IncomeExpensesEntity>> = _dailyIncomeExpensesByDateState*/

//    lateinit var dailyIncomeExpensesByDateState:List<IncomeExpensesEntity>


    init{
//        getIncomeExpenses()
      getDailyIncomeExpenses()
    }

     fun getDailyIncomeExpenses() {
        showLoading()
        dailyIncomeExpensesUseCase().onEach {
            delay(1000)
            _dailyIncomeExpensesState.value = it as MutableList<DailyModel>
            hideLoading()
        }.launchIn(viewModelScope)
    }

     fun getIncomeExpenses(){
        showLoading()
            getIncomeExpensesUseCase().onEach {
            _incomeExpensesState.value = it as MutableList<IncomeExpensesEntity>
                hideLoading()
            }.launchIn(viewModelScope)
        }

    fun updateIncomeExpensesEntityById(incomeExpenses: IncomeExpensesEntity){
        viewModelScope.launch {
            updateUseCase(incomeExpenses)
        }
    }

     fun getDailyIncomeExpensesByDate(date: Date) {
         showLoading()
         viewModelScope.launch {
             delay(1000)
             incomeExpensesByDateUseCase(date).let {
                 _incomeExpensesByDateState.value = it as MutableList<IncomeExpensesEntity>
                 hideLoading()
             }
         }
     }

     fun insertBothEntity(incomeExpenses: IncomeExpensesEntity){
        viewModelScope.launch {
            insertBothUseCase(incomeExpenses)
        }
    }

    fun deleteIncomeExpensesEntityByIdUseCase(id:Int){
        viewModelScope.launch {
            deleteIncomeExpensesByIdUseCase(id)
        }
    }

    fun isPriceEmpty(editText: EditText?):Double{
        return if(editText?.text.isNullOrEmpty())
            0.0
        else
            editText?.text.toString().toDouble()
    }

    fun isItemEmpty(editText: EditText?):Boolean{
        return if(editText?.text.isNullOrEmpty()){
            editText?.error = "empty"
            true
        }else
            false
    }

 /*   fun getCurrentDate():String{
        val c = Calendar.getInstance()
        val sdf = SimpleDateFormat("dd-MM-YYYY",Locale.US)
        return sdf.format(c.time)
    }*/
}