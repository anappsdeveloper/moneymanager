package com.krishnanepal.moneymanager.ui.home

import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.krishnanepal.moneymanager.R
import com.krishnanepal.moneymanager.data.local.IncomeExpensesEntity
import com.krishnanepal.moneymanager.databinding.ItemDetailListBinding

class ShowDetailAdapter(private val showDetailInterface:ShowDetailInterface,var list:MutableList<IncomeExpensesEntity>): RecyclerView.Adapter<ShowDetailAdapter.ViewHolder>() {
    lateinit var binding: ItemDetailListBinding

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ShowDetailAdapter.ViewHolder {
        binding = ItemDetailListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ShowDetailAdapter.ViewHolder, position: Int) {
        val item = list[position]
        binding.tvItem.text = item.item
        binding.tvExpenses.text = item.amt.toString()
        if (item.isExpenses)
            binding.tvExpenses.setTextColor(
                ContextCompat.getColor(
                    binding.tvExpenses.context,
                    R.color.expenses
                )
            )
        else
            binding.tvExpenses.setTextColor(
                ContextCompat.getColor(
                    binding.tvExpenses.context,
                    R.color.income
                )
            )

        //update list
        if(!list.isNullOrEmpty())
            showDetailInterface.sendUpdateList(list)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun updateList(filterOption: MutableList<IncomeExpensesEntity>) {
        list = filterOption
        notifyDataSetChanged()
    }

    inner class ViewHolder(var binding: ItemDetailListBinding) :
        RecyclerView.ViewHolder(binding.root) {
        init {
           /* if(!list.isNullOrEmpty())
                showDetailInterface.sendList(list)*/
//            Log.d("uytuytryt444",list.size.toString())
        }
    }

    interface ShowDetailInterface{
       fun sendUpdateList(list:MutableList<IncomeExpensesEntity>)
    }
}