package com.krishnanepal.moneymanager.ui.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.krishnanepal.moneymanager.data.model.DailyModel
import com.krishnanepal.moneymanager.databinding.ItemListBinding
import com.krishnanepal.moneymanager.utils.extentions.convertDateToString
import java.util.*

class HomeAdapter(
    private val list: MutableList<DailyModel>,
    private val homeAdapterInterface: HomeAdapterInterface,
    private val context: Context
): RecyclerView.Adapter<HomeAdapter.ViewHolder>(){

    lateinit var binding:ItemListBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
         binding = ItemListBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return ViewHolder(binding)
    }

    var count = -1
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list[position]
        if (position!= count){
         /** stop before arriving last index | current index date == next index date*/
            if (position < list.size - 1 && list[position].date == list[position + 1].date) {
                count = position + 1
                binding.tvDate.text = convertDateToString(item.date)
                /** if expenses; expenses as it is and put income in*/
                if(item.isExpenses){
                    binding.tvTotalIncome.text = list[position+1].sum.toString()
                    binding.tvTotalExpenses.text = item.sum.toString()
                }else{
                    binding.tvTotalIncome.text = item.sum.toString()
                    binding.tvTotalExpenses.text = list[position+1].sum.toString()
                }

            }else{
                binding.tvDate.text = convertDateToString(item.date)

                if(item.isExpenses){
                    binding.tvTotalExpenses.text = item.sum.toString()
                    binding.tvTotalIncome.text = "0"
                }else{
                    binding.tvTotalIncome.text = item.sum.toString()
                    binding.tvTotalExpenses.text = "0"
                }
            }
        }else{
            binding.container.visibility = View.GONE
        }

    }

    inner class ViewHolder(var binding: ItemListBinding) : RecyclerView.ViewHolder(binding.root) {
        init{
            binding.container.setOnClickListener {
                homeAdapterInterface?.onItemClick(list[adapterPosition].date)
            }
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun sort(){
        list.reverse()
        notifyDataSetChanged()
    }

    fun removeItem(position: Int) {
        list.removeAt(position)
        notifyItemRemoved(position)
    }


    interface HomeAdapterInterface{
        fun onItemClick(date: Date)
    }

}