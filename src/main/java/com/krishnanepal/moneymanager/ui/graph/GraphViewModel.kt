package com.krishnanepal.moneymanager.ui.graph

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.krishnanepal.moneymanager.base.BaseViewModel
import com.krishnanepal.moneymanager.data.local.IncomeExpensesEntity
import com.krishnanepal.moneymanager.data.model.DailyModel
import com.krishnanepal.moneymanager.domain.use_case.GetDailyIncomeExpensesUseCase
import com.krishnanepal.moneymanager.domain.use_case.GetIncomeExpensesUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class GraphViewModel
@Inject constructor(private val dailyIncomeExpensesUseCase: GetDailyIncomeExpensesUseCase,
) : BaseViewModel() {

    //date wise both sum of income and expenses show
    val _dailyIncomeExpensesState = MutableStateFlow(mutableListOf<DailyModel>())
    val dailyIncomeExpensesState: StateFlow<MutableList<DailyModel>> = _dailyIncomeExpensesState

    init {
        getDailyIncomeExpenses()
    }

    fun getDailyIncomeExpenses() {
        showLoading()
        dailyIncomeExpensesUseCase().onEach {
            delay(1000)
            _dailyIncomeExpensesState.value = it as MutableList<DailyModel>
            hideLoading()
        }.launchIn(viewModelScope)
    }

}