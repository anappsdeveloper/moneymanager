package com.krishnanepal.moneymanager.ui.graph

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.coroutineScope
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import com.google.gson.Gson
import com.krishnanepal.moneymanager.R
import com.krishnanepal.moneymanager.base.BaseFragment
import com.krishnanepal.moneymanager.data.model.DailyModel
import com.krishnanepal.moneymanager.databinding.GraphFragmentBinding
import com.krishnanepal.moneymanager.utils.extentions.convertDateToString
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import java.util.Calendar
import java.util.Date

@AndroidEntryPoint
class GraphFragment : BaseFragment<GraphFragmentBinding, GraphViewModel>() {
    private val grpahViewModel: GraphViewModel by viewModels()



    private val calendar: Calendar = Calendar.getInstance()
    private val year = calendar.get(Calendar.YEAR)


    override fun getLayoutId() = R.layout.graph_fragment

    override fun getViewModel():GraphViewModel = grpahViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initVM()
//        initUi()


    }

    private fun initUi(list: MutableList<DailyModel>) {
        val barEntries2 = ArrayList<BarEntry>()
        val barEntries1 = ArrayList<BarEntry>()
        val days = ArrayList<String>()

        var count = -1
        list.forEachIndexed() { index, item ->
            val dayAndMonth = item.date.toString().split(" ")
            if (index != count) {
                /** stop before arriving last index | current index date == next index date*/
                if (index < list.size - 1 && list[index].date == list[index + 1].date) {
                    count = index + 1

                    days.add(dayAndMonth.get(1) + " " + dayAndMonth.get(2))

                    if (item.isExpenses) {
                        barEntries2.add(BarEntry(index + 1.toFloat(), item.sum.toFloat()))
                        barEntries1.add(BarEntry(index + 1.toFloat(),list.get(index+1).sum.toFloat()))
                    } else {
                        barEntries1.add(BarEntry(index + 1.toFloat(), item.sum.toFloat()))
                        barEntries2.add(BarEntry(index + 1.toFloat(), list.get(index+1).sum.toFloat()))
                    }
                } else{
                    days.add(dayAndMonth.get(1) + " " + dayAndMonth.get(2))

                        if(item.isExpenses){
                            barEntries1.add(BarEntry(index + 1.toFloat(), 0.toFloat()))
                            barEntries2.add(BarEntry(index + 1.toFloat(), item.sum.toFloat()))
                        }else{
                            barEntries1.add(BarEntry(index + 1.toFloat(), item.sum.toFloat()))
                            barEntries2.add(BarEntry(index + 1.toFloat(), 0.toFloat()))
                        }
                    }
                }

            }
            val barDataSet1 = BarDataSet(barEntries1, "Income")
            barDataSet1.setColor(Color.GREEN)
            val barDataSet2 = BarDataSet(barEntries2, "Expenses")
            barDataSet2.setColor(Color.RED)

            val barData = BarData(barDataSet1, barDataSet2)

            with(binding) {


                barChart.data = barData

//            val days = listOf("Sunday", "Monday")
                val xAsis = binding.barChart.xAxis
                xAsis.valueFormatter = IndexAxisValueFormatter(days)
                xAsis.setCenterAxisLabels(true)
                xAsis.position = XAxis.XAxisPosition.BOTTOM
                xAsis.granularity = 1f
                xAsis.isGranularityEnabled = true

                barChart.isDragEnabled = true
                barChart.setVisibleXRangeMaximum(3f)

//        val barSpace = 0.08f
                val barSpace = 0.38f
                var groupSpace = 0.04f
                barData.barWidth = 0.10f

                barChart.xAxis.axisMinimum = 0f
                barChart.xAxis.mAxisMaximum =
                    0f + barChart.barData.getGroupWidth(groupSpace, barSpace) * 2
                barChart.axisLeft.axisMinimum = 0f

                barChart.groupBars(0f, groupSpace, barSpace)
                barChart.invalidate()
            }

            /*** fromula inorder to align graph
             * (barwidth + barspace) * no of bar + group space = 1
             *
             */

        }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.main_menu, menu);

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return when (item.itemId) {
            android.R.id.home -> {
                activity?.onBackPressed()
                true
            }
            R.id.logout -> {
                Toast.makeText(activity,"Logout", Toast.LENGTH_SHORT).show()
                true
            }
           /* R.id.search -> {

                true
            }*/
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun initVM() {
        lifecycle.coroutineScope.launchWhenStarted {
            grpahViewModel.dailyIncomeExpensesState.collect {
                if(it.isNullOrEmpty()){
                    binding.layoutDataNotFound.root.visibility = View.VISIBLE
                    binding.root.visibility = View.GONE
                }else{
                    binding.layoutDataNotFound.root.visibility = View.GONE
                    binding.root.visibility = View.VISIBLE
                    initUi(it)
                    Log.d("GraphFragmanetTag", Gson().toJson(it))
                  /*  homeAdapter = HomeAdapter(it, this@HomeFragment,requireActivity())
                    binding.rv.adapter = homeAdapter*/
                }
            }
        }

        /*  lifecycle.coroutineScope.launchWhenStarted {
              homeViewModel.incomeExpensesState.collect {
                  if(it.isNullOrEmpty()){
                      binding.layoutDataNotFound.root.visibility = View.VISIBLE
                      binding.root.visibility = View.GONE
                  }else{
                      Log.d("fdsfdfd", Gson().toJson(it))
                      binding.layoutDataNotFound.root.visibility = View.GONE
                      binding.root.visibility = View.VISIBLE
                      showDetailAdapter = ShowDetailAdapter(this@HomeFragment,it)
                      binding.rv.adapter = showDetailAdapter
                  }
              }
          }*/
    }


    private fun barEntries1():ArrayList<BarEntry>{
        val barEntries = ArrayList<BarEntry>()
        barEntries.add(BarEntry(1f,200f))
        barEntries.add(BarEntry(2f,500f))


return  barEntries
    }

    private fun barEntries2():ArrayList<BarEntry>{
        val barEntries = ArrayList<BarEntry>()
        barEntries.add(BarEntry(1f,400f))
        barEntries.add(BarEntry(2f,100f))


        return  barEntries
    }
}