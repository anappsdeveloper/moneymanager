package com.krishnanepal.moneymanager.ui.login

import android.graphics.Paint
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.krishnanepal.moneymanager.R
import com.krishnanepal.moneymanager.base.BaseFragment
import com.krishnanepal.moneymanager.databinding.FragmentLoginBinding
import com.krishnanepal.moneymanager.utils.Constants.Strings.ERROR
import com.krishnanepal.moneymanager.utils.Constants.Strings.INFO
import com.krishnanepal.moneymanager.utils.Constants.Strings.SUCCESS
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginFragment : BaseFragment<FragmentLoginBinding, LoginViewModel>() {


    private val loginViewModel: LoginViewModel by viewModels()
    private lateinit var auth: FirebaseAuth
   lateinit var userName:String
   lateinit var password:String

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        FirebaseApp.initializeApp(requireActivity())
        auth = FirebaseAuth.getInstance()
        binding.btnLogin.setOnClickListener {
            userName = binding.etUsername.text.toString()
            password = binding.etPassword.text.toString()
            if (userName.length > 6) {
//                systemPrefManager.save(SystemPreferenceManager.USER_NAME, userName)
//                startActivity(Intent(this@WelcomeActivity, MainActivity::class.java))
                hitFirebase(userName,password)
            }else{
                showToastMessage("Please enter valid name", INFO)
            }
        }
        binding.tvRegister.setPaintFlags(binding.tvRegister.getPaintFlags() or Paint.UNDERLINE_TEXT_FLAG)
        binding.tvRegister.setOnClickListener {
            findNavController().navigate(R.id.action_loginFragment_to_signupFragment)
        }
    }

    private fun hitFirebase(userName: String, password: String) {

        auth.signInWithEmailAndPassword(userName, password)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d("LoginFragmentTag", "createUserWithEmail:success")
                    val user = auth.currentUser
                    findNavController().navigate(R.id.action_loginFragment_to_mainFragment)
                    showToastMessage("Authenticate success", SUCCESS)
//                    updateUI(user)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.d("LoginFragmentTag", "createUserWithEmail:failure", task.exception)
                    showToastMessage("Authenticate failed", ERROR)
//                    updateUI(null)
                }

            }
    }

    override fun onStart() {
        super.onStart()
        val currentUser = auth.currentUser
        if(currentUser!=null){
            Log.d("LoginFragmentTag", "currentuser")
        }
    }

    override fun getLayoutId() = R.layout.fragment_login

    override fun getViewModel() = loginViewModel
}